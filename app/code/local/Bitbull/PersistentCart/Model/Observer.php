<?php
class Bitbull_PersistentCart_Model_Observer extends Mage_Core_Model_Observer
{

    /**
     * Remove quote persistance before success page 
     * &
     * Reset cart product quantity counter
     * 
     * @param Varien_Event_Observer $observer
     */
    public function unsetPersistency(Varien_Event_Observer $observer)
    {
        $orderId = $observer->getOrderIds();
        $order = Mage::getModel('sales/order')->load($orderId[0]);
        $quoteId = $order->getQuoteId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);
        $quote->setIsActive(false);
        $quote->save();

        //reset header cart product quantities
        $minicart = Mage::app()->getLayout()->getBlock('top_cart');
        if ($minicart) {
            $minicart->setData("summary_qty", "-1");
        }
    }


    /**
     * Maintain quote persistent afeter submitOrder()
     *
     * @param Varien_Event_Observer $observer
     */
    public function setPersistency(Varien_Event_Observer $observer)
    {
        $quote = $observer->getQuote();
        $quote->setIsActive(true)->save();
    }
}