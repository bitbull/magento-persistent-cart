<?php
/**
 * @author Manuele Menozzi <mmenozzi@webgriffe.com>
 */

class Webgriffe_CustomStockUpdate_Model_System_Config_Source_Order_StateStatus
{
    const STATE_STATUS_SEPARATOR = '::';

    protected $_selectableStates = array(
        Mage_Sales_Model_Order::STATE_NEW,
        Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
        Mage_Sales_Model_Order::STATE_PROCESSING,
        Mage_Sales_Model_Order::STATE_COMPLETE,
        Mage_Sales_Model_Order::STATE_CLOSED,
        Mage_Sales_Model_Order::STATE_CANCELED,
        Mage_Sales_Model_Order::STATE_HOLDED,
        Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW,
    );

    public function toOptionArray()
    {
        $options = array();

        foreach ($this->_selectableStates as $state) {
            $optionGroup = array();
            $statuses = Mage::getModel('sales/order_status')
                ->getCollection()
                ->addStateFilter($state);
            foreach ($statuses as $status) {
                $optionGroup[] = array(
                    'label' => $status->getLabel(),
                    'value' => sprintf('%s%s%s', $state, self::STATE_STATUS_SEPARATOR, $status->getStatus())
                );
            }

            $options[] = array(
                'label' => Mage::getSingleton('sales/order_config')->getStateLabel($state),
                'value' => $optionGroup,
            );
        }

        return $options;
    }
}
