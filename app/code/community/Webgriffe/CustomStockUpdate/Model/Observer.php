<?php
/**
 * Created by PhpStorm.
 * User: andrea
 * Date: 02/05/14
 * Time: 16.38
 */

/**
 * Class Webgriffe_CustomStockUpdate_Model_Observer
 */
class Webgriffe_CustomStockUpdate_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     *
     * @See Mage_CatalogInventory_Model_Observer::cancelOrderItem
     */
    public function onItemCancel(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order_Item $item */
        $item = $observer->getEvent()->getItem();

        if (!Mage::helper('wg_stock')->canIncreaseStock($item)) {
            $children = $item->getChildrenItems();
            $qty = $item->getQtyOrdered() -
                max($item->getQtyShipped(), $item->getQtyInvoiced()) -
                $item->getQtyCanceled();

            $productId = $item->getProductId();
            $delta = 0;
            if ($item->getId() && $productId && empty($children) && $qty) {
                $delta = $qty;
            }

            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
            $stockItem->subtractQty($delta)->save();
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     *
     * @See Mage_CatalogInventory_Model_Observer::subtractQuoteInventory
     */
    public function onQuoteSubmit(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();

        if (!Mage::helper('wg_stock')->canReduceStock($order)) {
            Mage::helper('wg_stock')->revertQuoteStock($observer->getEvent()->getQuote());
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     *
     * @See Mage_CatalogInventory_Model_Observer::checkoutAllSubmitAfter
     */
    public function onCheckoutSubmit(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();
        if (is_null($order)) {
            $orders = $observer->getEvent()->getOrders();
            $order = $orders[0];
        }

        if (!Mage::helper('wg_stock')->canReduceStock($order)) {
            Mage::helper('wg_stock')->revertQuoteStock($observer->getEvent()->getQuote());
        }
    }

    public function onOrderSave(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        if (Mage::helper('wg_stock')->isCriticalOrderStateChange($order)) {
            Mage::helper('wg_stock')->handleCriticalOrderStateChange($order);
        }
    }
}
