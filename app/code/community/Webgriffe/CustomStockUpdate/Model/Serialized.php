<?php

class Webgriffe_CustomStockUpdate_Model_Serialized extends Mage_Adminhtml_Model_System_Config_Backend_Serialized_Array
{
    protected function _afterLoad()
    {
        $value = unserialize($this->getValue());

        $result = array();
        foreach ($value as $paymentMethod => $data) {
            $data['payment_method'] = $paymentMethod;
            $result[] = $data;
        }

        $this->setValue($result);
    }

    protected function _beforeSave()
    {
        $value = $this->getValue();
        if (!is_array($value)) {
            return;
        }

        unset($value['__empty']);

        $result = array();
        foreach ($value as $data) {
            $paymentMethod = $data['payment_method'];
            if (empty($paymentMethod)) {
                continue;
            }
            unset($data['payment_method']);
            $result[$paymentMethod] = $data;
        }

        $this->setValue(serialize($result));
    }
}
