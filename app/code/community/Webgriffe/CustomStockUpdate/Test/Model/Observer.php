<?php
/**
 * Created by PhpStorm.
 * User: andrea
 * Date: 06/05/14
 * Time: 11.43
 */

class Webgriffe_CustomStockUpdate_Test_Model_Observer extends EcomDev_PHPUnit_Test_Case_Controller
{
    public function setUp()
    {
        $this->setCurrentStore('default');
    }

    /**
     * @loadFixture moduleConfig.yaml
     */
    public function testPlaceOrder()
    {
        $formKey = '69FWke2LV4Dyy3lK';
        $this->assertCount(0, Mage::getModel('sales/order')->getCollection()->getItems());

        $this->disableSessionInitialization('checkout/session');
        $this->disableSessionInitialization('customer/session');
        $this->disableSessionInitialization('core/session', array('getFormKey'))
            ->expects($this->any())
            ->method('getFormKey')
            ->will($this->returnValue($formKey));

        $this->prepareQuoteForOrderCreation();

        $product = $this->getProduct();
        $this->assertTrue($product->isAvailable());
        $this->assertTrue($product->isInStock());
        /** @var Mage_CatalogInventory_Model_Stock_Item $stockItem */
        $stockItem = $product->getStockItem();
        $stockItem->load($stockItem->getId());
        $this->assertNotNull($stockItem);
        $this->assertEquals(10, $stockItem->getQty());
        $this->assertTrue((bool)$stockItem->getIsInStock());

        $this->markTestSkipped('La chiamata seguente fa andare rossi due test di ecomdev');

        self::getRequest()->setMethod('POST');
        $this->dispatch('checkout/onepage/saveOrder/form_key/'.$formKey);

        $orders = Mage::getModel('sales/order')->getCollection()->getItems();
        $this->assertCount(1, $orders);

        /** @var Mage_Sales_Model_Order $order */
        $order = array_shift($orders);
        $this->assertNotNull($order);
        $orderId = $order->getId();
        $this->assertNotNull($orderId);
        $this->assertTrue(is_numeric($orderId));
        $this->assertEquals('new', $order->getState());
        $this->assertEquals('pending', $order->getStatus());

        $product = $this->getProduct();
        $this->assertTrue($product->isAvailable());
        $this->assertTrue($product->isInStock());
        $stockItem = $product->getStockItem();
        $stockItem->load($stockItem->getId());
        $this->assertNotNull($stockItem);
        $this->assertEquals(10, $stockItem->getQty());
        $this->assertTrue((bool)$stockItem->getIsInStock());

        $this->markTestIncomplete();

        $this->mockAdminUserSession();
        self::getRequest()->setMethod('POST');
        $this->dispatch(
            "adminhtml/sales_order_shipment/save/order_id/$orderId/key/$formKey",
            array('_store' => 'admin')
        );

        $orders = Mage::getModel('sales/order')->getCollection()->getItems();
        $this->assertCount(1, $orders);

        /** @var Mage_Sales_Model_Order $order */
        $order = array_shift($orders);
        $this->assertNotNull($order);
        $orderId = $order->getId();
        $this->assertNotNull($orderId);
        $this->assertTrue(is_numeric($orderId));
        $this->assertEquals('processing', $order->getState());
        $this->assertEquals('processing', $order->getStatus());

        $product = $this->getProduct();
        $this->assertTrue($product->isAvailable());
        $this->assertTrue($product->isInStock());
        $stockItem = $product->getStockItem();
        $stockItem->load($stockItem->getId());
        $this->assertNotNull($stockItem);
        $this->assertEquals(10, $stockItem->getQty());
        $this->assertTrue((bool)$stockItem->getIsInStock());

        //TODO: spedire l'ordine e controllare lo stock
        //TODO: fatturare l'ordine e controllare lo stock

        //TODO: in un altro test spedire e cancellare l'ordine e controllare lo stock

        //TODO: aggiungere un data provider con le possibili scelte quanto a metodo di pagamento e relativi risultati attesi
    }

    public function tearDown()
    {
        Mage::register('isSecureArea', true, true);

        $this->clearModelInstances('sales/order');
        $this->clearModelInstances('sales/quote');
    }

    private function prepareQuoteForOrderCreation()
    {
        $product = $this->getProduct();
        $this->assertNotNull($product->getId());

        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $quote->addProduct($product, 10);
        $quote->save();

        $billingAddress = Mage::getModel('sales/quote_address');

        $billingAddress->setRegion('Reggio Emilia');
        $billingAddress->setStreetFull('via Canale 1/P');
        $billingAddress->setCity('Casalgrande');
        $billingAddress->setCompany('Webgriffe');
        $billingAddress->setEmail('support@webgriffe.com');
        $billingAddress->setFirstname('Andrea');
        $billingAddress->setLastname('Zambon');
        $billingAddress->setPostcode(42013);
        $billingAddress->setPrefix(0522);
        $billingAddress->setTelephone(123456);
        $billingAddress->setCountryId('IT');
        $billingAddress->setAddressType('billing');
        $billingAddress->setQuote($quote);
        $billingAddress->save();

        $quote->setBillingAddress($billingAddress);

        $shippingAddress = clone $billingAddress;
        $shippingAddress->setAddressType('shipping');
        $shippingAddress->setShippingMethod('dhl');
        $shippingAddress->save();

        $shippingRate = Mage::getModel('sales/quote_address_rate');
        $shippingRate->setAddress($shippingAddress);
        $shippingRate->setRate(5);
        $shippingRate->setCode('dhl');
        $shippingRate->save();

        $shippingAddress->setRate($shippingRate);
        $shippingAddress->save();

        $quote->setShippingAddress($shippingAddress);
        $quote->save();

        $quote->getPayment()->setMethod('checkmo');
        $quote->getPayment()->save();

        $quote->collectTotals()->save();
    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    private function getProduct()
    {
        return Mage::getModel('catalog/product')->load(1);
    }

    private function clearModelInstances($model)
    {
        foreach (Mage::getModel($model)->getCollection() as $instance) {
            $instance->delete();
        }
    }

    /**
     * @param $classAlias
     * @param array $additionalMethods
     *
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function disableSessionInitialization($classAlias, $additionalMethods = array())
    {
        //In ambiente di test non è possibile usare l'inizializzazione della sessione di Magento, dato che questa cerca
        //di eseguire un session_start() che da errore a causa degli header già inviati
        $sessionMock = $this->getModelMockBuilder($classAlias)
            ->setMethods(array_merge(array('start'), $additionalMethods))
            ->getMock();

        $sessionMock->expects($this->any())->method('start')->will($this->returnSelf());

        $this->replaceByMock('singleton', $classAlias, $sessionMock);
        $this->replaceByMock('model', $classAlias, $sessionMock);

        return $sessionMock;
    }
}
