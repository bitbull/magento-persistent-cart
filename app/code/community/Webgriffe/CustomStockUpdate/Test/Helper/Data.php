<?php
/**
 * Created by PhpStorm.
 * User: andrea
 * Date: 06/05/14
 * Time: 11.56
 */

class Webgriffe_CustomStockUpdate_Test_Helper_Data extends EcomDev_PHPUnit_Test_Case
{
    public function setUp()
    {
        $this->setCurrentStore('default');
    }

    public function checkOrderChangeStatusDataProvider()
    {
        return array(
            array(null, null, 'new', 'pending', 'checkmo', false),
            array(null, null, 'processing', 'processing', 'checkmo', true),
            array('new', 'pending', 'processing', 'processing', 'checkmo', true),
            array('processing', 'processing', 'processing', 'processing', 'checkmo', false),
            array('processing', 'processing', 'complete', 'complete', 'checkmo', false),
            array('processing', 'processing', 'canceled', 'canceled', 'checkmo', true),
            array('complete', 'complete', 'canceled', 'canceled', 'checkmo', true),

            array(null, null, 'new', 'pending', 'cc', false),
            array(null, null, 'processing', 'processing', 'cc', false),
            array('new', 'pending', 'processing', 'processing', 'cc', false),
            array('processing', 'processing', 'processing', 'processing', 'cc', false),
            array('processing', 'processing', 'complete', 'complete', 'cc', true),
            array('processing', 'processing', 'canceled', 'canceled', 'cc', true),
            array('complete', 'complete', 'canceled', 'canceled', 'cc', false),

            array(null, null, 'new', 'pending', 'paypal', false),
            array(null, null, 'processing', 'processing', 'paypal', false),
            array('new', 'pending', 'processing', 'processing', 'paypal', false),
            array('processing', 'processing', 'processing', 'processing', 'paypal', false),
            array('processing', 'processing', 'complete', 'complete', 'paypal', false),
            array('processing', 'processing', 'canceled', 'canceled', 'paypal', false),
            array('complete', 'complete', 'canceled', 'canceled', 'paypal', false),
        );
    }

    /**
     * @dataProvider checkOrderChangeStatusDataProvider
     * @loadFixture moduleConfig.yaml
     */
    public function testCheckOrderChangeStatus($fromState, $fromStatus, $toState, $toStatus, $paymentMethod, $result)
    {
        $order = $this->createOrder($fromState, $fromStatus, $toState, $toStatus, $paymentMethod);

        $this->assertEquals($result, Mage::helper('wg_stock')->isCriticalOrderStateChange($order));
    }

    /**
     * @dataProvider checkOrderChangeStatusDataProvider
     * @loadFixture disabledModuleConfig.yaml
     */
    public function testCheckDisabledModuleOrderChangeStatus(
        $fromState,
        $fromStatus,
        $toState,
        $toStatus,
        $paymentMethod
    ) {
        $order = $this->createOrder($fromState, $fromStatus, $toState, $toStatus, $paymentMethod);

        $this->assertEquals(false, Mage::helper('wg_stock')->isCriticalOrderStateChange($order));
    }

    /**
     * @dataProvider checkOrderChangeStatusDataProvider
     * @loadFixture moduleConfig.yaml
     */
    public function testHandleCriticalStateChange($fromState, $fromStatus, $toState, $toStatus, $paymentMethod, $result)
    {
        $order = $this->createOrderWithOrderItem($fromState, $fromStatus, $toState, $toStatus, $paymentMethod);
        $items = $order->getAllItems();
        $item = $items[0];

        $stock = $this->getModelMockBuilder('cataloginventory/stock');
        $stock->addMethod('revertProductsSale', 'registerProductsSale');
        $indexer = $this->getResourceModelMockBuilder('cataloginventory/indexer_stock');
        $indexer->addMethod('reindexProducts');

        $stockItem = $this->getModelMockBuilder('cataloginventory/stock_item')->addMethod('save');
        $item->getProduct()->setStockItem($stockItem);

        if ($result) {
            $stock->expects($this->any())
                ->method('registerProductsSale')
                ->with(array('1' => array('item' => $item->getProduct()->getStockItem(), 'qty' => 3)));
            $stock->expects($this->any())
                ->method('revertProductsSale')
                ->with(array('1' => array('item' => $item->getProduct()->getStockItem(), 'qty' => 3)));

            $indexer->expects($this->once())->method('reindexProducts')->with(array('1' => '1'));
        } else {
            $stock->expects($this->never())->method('registerProductsSale');
            $stock->expects($this->never())->method('revertProductsSale');
            $indexer->expects($this->never())->method('reindexProducts');
        }

        $this->replaceByMock('singleton', 'cataloginventory/stock', $stock);
        $this->replaceByMock('resource_singleton', 'cataloginventory/indexer_stock', $indexer);

        $helper = Mage::helper('wg_stock');
        if ($helper->isCriticalOrderStateChange($order)) {
            $helper->handleCriticalOrderStateChange($order);
        }
    }

    /**
     * @param $fromState
     * @param $fromStatus
     * @param $toState
     * @param $toStatus
     * @param $paymentMethod
     * @return Mage_Sales_Model_Order
     */
    private function createOrder($fromState, $fromStatus, $toState, $toStatus, $paymentMethod)
    {
        $payment = Mage::getModel('sales/order_payment');
        $payment->setMethod($paymentMethod);

        $order = Mage::getModel('sales/order');
        $order->setData('state', $toState);
        $order->setData('status', $toStatus);
        $order->setOrigData('state', $fromState);
        $order->setOrigData('status', $fromStatus);
        $order->setPayment($payment);

        return $order;
    }

    /**
     * @param $fromState
     * @param $fromStatus
     * @param $toState
     * @param $toStatus
     * @param $paymentMethod
     * @return Mage_Sales_Model_Order
     */
    private function createOrderWithOrderItem($fromState, $fromStatus, $toState, $toStatus, $paymentMethod)
    {
        $item = Mage::getModel('sales/order_item');
        $item->setQtyOrdered(3);
        $item->setQtyCanceled(0);
        $item->setQtyRefunded(0);
        $item->setProductId(1);
        $order = $this->createOrder($fromState, $fromStatus, $toState, $toStatus, $paymentMethod);
        $order->addItem($item);

        return $order;
    }
}
