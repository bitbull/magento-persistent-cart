<?php
/**
 * Created by PhpStorm.
 * User: andrea
 * Date: 02/05/14
 * Time: 14.39
 */

class Webgriffe_CustomStockUpdate_Helper_Data extends Mage_Core_Helper_Abstract
{
    const ACTIVE_SWITCH_CONFIG_PATH                 = 'sales/webgriffe_stock_general/active';
    const PAYMENT_METHODS_LIST_CONFIG_PATH          = 'sales/webgriffe_stock_general/methods';
    const DEFAULT_ORDER_STATE_STATUS_CONFIG_PATH    = 'sales/webgriffe_stock_general/default_order_statuses';
    const ORDER_STATE_STATUS_EXCEPTIONS_CONFIG_PATH = 'sales/webgriffe_stock_general/exceptions';

    public function canReduceStock(Mage_Sales_Model_Order $order)
    {
        return $this->canUpdateStock($order);
    }

    public function canIncreaseStock(Mage_Sales_Model_Order_Item $item)
    {
        return $this->canUpdateStock($item->getOrder());
    }

    public function revertQuoteStock(Mage_Sales_Model_Quote $quote)
    {
        if (!$this->isModuleActive()) {
            return;
        }

        if (!$quote->getInventoryProcessed()) {
            return;
        }

        $observer = new Varien_Object(array('event' => new Varien_Object(array('quote' => $quote))));
        Mage::getSingleton('cataloginventory/observer')->reindexQuoteInventory($observer);

        $items = $quote->getAllItems();
        $qtys = $this->getProductsQty($items);
        Mage::getSingleton('cataloginventory/stock')->revertProductsSale($qtys);
        $this->reindexInventory($items);
        $this->resaveStockItems($items);
        $quote->setInventoryProcessed(false);
    }

    public function isCriticalOrderStateChange(Mage_Sales_Model_Order $order)
    {
        if (!$this->isModuleActive()) {
            return false;
        }

        $method = $order->getPayment()->getMethod();
        if (!$this->isAffectedPaymentMethod($method)) {
            return false;
        }

        //Se si passa da uno stato non tra quelli elencati a uno di quelli indicati (o viceversa), allora ritorna true
        return $this->checkStateStatus($order->getOrigData('state'), $order->getOrigData('status'), $method) !==
                $this->checkStateStatus($order->getState(), $order->getStatus(), $method);
    }

    public function handleCriticalOrderStateChange(Mage_Sales_Model_Order $order)
    {
        if (!$this->isModuleActive()) {
            return;
        }

        $method = $order->getPayment()->getMethod();
        if (!$this->isAffectedPaymentMethod($method)) {
            return;
        }

        if ($this->checkStateStatus($order->getState(), $order->getStatus(), $method)) {
            //Da stato non tra quelli specificati a stato tra quelli specificati. Riduci lo stock
            $this->reduceOrderStock($order);
            return;
        }

        //Da stato tra quelli specificati a stato non tra quelli specificati: ripristina lo stock
        $this->revertOrderStock($order);
    }

    private function reduceOrderStock(Mage_Sales_Model_Order $order)
    {
        if (!$this->isModuleActive()) {
            return;
        }

        //TODO: getAllItems o getAllVisibleItems?
        $items = $order->getAllItems();
        $qtys = $this->getProductsQty($items);
        Mage::getSingleton('cataloginventory/stock')->registerProductsSale($qtys);
        $this->reindexInventory($items);
        $this->resaveStockItems($items);
    }

    private function revertOrderStock(Mage_Sales_Model_Order $order)
    {
        if (!$this->isModuleActive()) {
            return;
        }

        //TODO: getAllItems o getAllVisibleItems?
        $items = $order->getAllItems();
        $qtys = $this->getProductsQty($items);
        Mage::getSingleton('cataloginventory/stock')->revertProductsSale($qtys);
        $this->reindexInventory($items);
        $this->resaveStockItems($items);
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    private function canUpdateStock(Mage_Sales_Model_Order $order)
    {
        if (!$this->isModuleActive()) {
            return true;
        }

        $method = $order->getPayment()->getMethod();
        if (!$this->isAffectedPaymentMethod($method)) {
            return true;
        }

        return $this->checkStateStatus($order->getState(), $order->getStatus(), $method);
    }

    /**
     * @param string $orderState
     * @param string $orderStatus
     * @param string $method
     *
     * @return bool
     */
    private function checkStateStatus($orderState, $orderStatus, $method)
    {
        $stateStatuses = explode(',', Mage::getStoreConfig(self::DEFAULT_ORDER_STATE_STATUS_CONFIG_PATH));

        $exceptions = unserialize(Mage::getStoreConfig(self::ORDER_STATE_STATUS_EXCEPTIONS_CONFIG_PATH));
        if (is_array($exceptions) && array_key_exists($method, $exceptions)) {
            $stateStatuses = $exceptions[$method]['order_statuses'];
        }

        foreach ($stateStatuses as $stateStatus) {
            if (empty($stateStatus)) {
                continue;
            }

            list($state, $status) = explode('::', $stateStatus);
            if ($orderState == $state && $orderStatus == $status) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return boolean
     */
    private function isModuleActive()
    {
        return Mage::getStoreConfig('cataloginventory/options/can_subtract') &&
            Mage::getStoreConfig(self::ACTIVE_SWITCH_CONFIG_PATH);
    }

    private function isAffectedPaymentMethod($method)
    {
        return in_array($method, explode(',', Mage::getStoreConfig(self::PAYMENT_METHODS_LIST_CONFIG_PATH)));
    }

    /**
     * Prepare array with information about used product qty and product stock item
     * result is:
     * array(
     *  $productId  => array(
     *      'qty'   => $qty,
     *      'item'  => $stockItems|null
     *  )
     * )
     * @param array $relatedItems
     * @return array
     */
    private function getProductsQty($relatedItems)
    {
        $items = array();
        /** @var Mage_Sales_Model_Quote_Item|Mage_Sales_Model_Order_Item $item */
        foreach ($relatedItems as $item) {
            $productId  = $item->getProductId();
            if (!$productId) {
                continue;
            }
            $children = $item->getChildrenItems();
            if ($children) {
                foreach ($children as $childItem) {
                    $this->addItemToQtyArray($childItem, $items);
                }
            } else {
                $this->addItemToQtyArray($item, $items);
            }
        }
        return $items;
    }

    /**
     * Adds stock item qty to $items (creates new entry or increments existing one)
     * $items is array with following structure:
     * array(
     *  $productId  => array(
     *      'qty'   => $qty,
     *      'item'  => $stockItems|null
     *  )
     * )
     *
     * @param Mage_Sales_Model_Quote_Item|Mage_Sales_Model_Order_Item $item
     * @param array &$items
     */
    private function addItemToQtyArray($item, &$items)
    {
        $productId = $item->getProductId();
        if (!$productId) {
            return;
        }

        if (isset($items[$productId])) {
            $items[$productId]['qty'] += $this->getItemQty($item);
            return;
        }

        $stockItem = null;
        if ($item->getProduct()) {
            $stockItem = $item->getProduct()->getStockItem();
        }

        $items[$productId] = array(
            'item' => $stockItem,
            'qty'  => $this->getItemQty($item)
        );
    }

    /**
     * @param Mage_Sales_Model_Quote_Item|Mage_Sales_Model_Order_Item $item
     * @return float|int
     */
    private function getItemQty($item)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Item) {
            return $item->getTotalQty();
        }

        /** @var Mage_Sales_Model_Order_Item $item */
        return $item->getQtyOrdered() - $item->getQtyRefunded() - $item->getQtyCanceled();
    }

    private function reindexInventory($items)
    {
        // Reindex ids
        $productIds = array();
        /** @var Mage_Sales_Model_Quote_Item|Mage_Sales_Model_Order_Item $item */
        foreach ($items as $item) {
            $productIds[$item->getProductId()] = $item->getProductId();
            $children = $item->getChildrenItems();
            if ($children) {
                /** @var Mage_Sales_Model_Quote_Item|Mage_Sales_Model_Order_Item $childItem */
                foreach ($children as $childItem) {
                    $productIds[$childItem->getProductId()] = $childItem->getProductId();
                }
            }
        }

        if (count($productIds)) {
            Mage::getResourceSingleton('cataloginventory/indexer_stock')->reindexProducts($productIds);
        }
    }

    /**
     * @param $items
     */
    private function resaveStockItems($items)
    {
        foreach ($items as $item) {
            /** @var Mage_Sales_Model_Quote_Item|Mage_Sales_Model_Order_Item $item */
            if (!$item->getProduct()) {
                continue;
            }

            /** @var Mage_CatalogInventory_Model_Stock_Item $stockItem */
            $stockItem = $item->getProduct()->getStockItem();
            $stockItem->load($stockItem->getId());
            $stockItem->setIsInStock($stockItem->verifyStock());
            $stockItem->setStockStatusChangedAutomaticallyFlag(false);
            $stockItem->save();
        }
    }
}
