<?php
/**
 * @author Manuele Menozzi <mmenozzi@webgriffe.com>
 */

class Webgriffe_CustomCouponUpdate_Test_Config_General extends EcomDev_PHPUnit_Test_Case_Config
{
    public function testModuleDependsFromSalesRule()
    {
        $this->assertModuleDepends('Mage_SalesRule');
    }

    public function testModuleDependsFromWebgriffeCustomStockUpdate()
    {
        $this->assertModuleDepends('Webgriffe_CustomStockUpdate');
    }

    public function testObserverIsDefined()
    {
        $this->assertEventObserverDefined(
            'global',
            'sales_order_place_after',
            'wg_customcouponupdate/observer',
            'salesOrderPlaceAfter'
        );
        $this->assertEventObserverDefined(
            'global',
            'sales_order_save_after',
            'wg_customcouponupdate/observer',
            'salesOrderSaveAfter'
        );
    }

    public function testCouponUsageResourceModelIsDefined()
    {
        $this->assertResourceModelAlias(
            'wg_customcouponupdate/coupon_usage',
            'Webgriffe_CustomCouponUpdate_Model_Resource_Coupon_Usage'
        );
    }
} 