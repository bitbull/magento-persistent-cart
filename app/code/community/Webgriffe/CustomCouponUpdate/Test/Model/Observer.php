<?php
/**
 * @author Manuele Menozzi <mmenozzi@webgriffe.com>
 */

class Webgriffe_CustomCouponUpdate_Test_Model_Observer extends EcomDev_PHPUnit_Test_Case
{
    public function testClassExists()
    {
        $this->assertTrue(class_exists('Webgriffe_CustomCouponUpdate_Model_Observer'));
    }

    public function testSalesOrderPlaceAfterShouldRevertCouponUsage()
    {
        $order = new Mage_Sales_Model_Order();

        $this->createCanReduceStockWgStockHelperMock($order, false);
        $this->createCouponManagerMock($order, true);

        $observerData = new Varien_Object(array('event' => new Varien_Object(array('order' => $order))));
        $observer = new Webgriffe_CustomCouponUpdate_Model_Observer();
        $observer->salesOrderPlaceAfter($observerData);
    }

    public function testSalesOrderPlaceAfterShouldNotRevertCouponUsage()
    {
        $order = new Mage_Sales_Model_Order();

        $this->createCanReduceStockWgStockHelperMock($order, true);
        $this->createCouponManagerMock($order, false);

        $observerData = new Varien_Object(array('event' => new Varien_Object(array('order' => $order))));
        $observer = new Webgriffe_CustomCouponUpdate_Model_Observer();
        $observer->salesOrderPlaceAfter($observerData);
    }

    public function testSalesOrderSaveAfterShouldApplyCouponUsage()
    {
        $order = new Mage_Sales_Model_Order();

        $this->createIsCriticalOrderStateChangeWgStockHelperMock($order, true);

        $observerData = new Varien_Object(array('event' => new Varien_Object(array('order' => $order))));

        $this->createSalesRuleObserverMock($observerData, true);

        $observer = new Webgriffe_CustomCouponUpdate_Model_Observer();
        $observer->salesOrderSaveAfter($observerData);
    }

    public function testSalesOrderSaveAfterShouldNotApplyCouponUsage()
    {
        $order = new Mage_Sales_Model_Order();

        $this->createIsCriticalOrderStateChangeWgStockHelperMock($order, false);

        $observerData = new Varien_Object(array('event' => new Varien_Object(array('order' => $order))));

        $this->createSalesRuleObserverMock($observerData, false);

        $observer = new Webgriffe_CustomCouponUpdate_Model_Observer();
        $observer->salesOrderSaveAfter($observerData);
    }

    /**
     * @param $order
     * @param $canReduceStock
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function createCanReduceStockWgStockHelperMock($order, $canReduceStock)
    {
        $wgStockHelperMock = $this->getMock('Webgriffe_CustomStockUpdate_Helper_Data');
        $wgStockHelperMock
            ->expects($this->once())
            ->method('canReduceStock')
            ->with($order)
            ->will($this->returnValue($canReduceStock));
        $this->replaceByMock('helper', 'wg_stock', $wgStockHelperMock);

        return $wgStockHelperMock;
    }

    /**
     * @param $order
     * @param $shouldRevert bool
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function createCouponManagerMock($order, $shouldRevert)
    {
        $couponManagerMock = $this->getMock('Webgriffe_CustomCouponUpdate_Helper_CouponManager');
        if ($shouldRevert) {
            $couponManagerMock
                ->expects($this->once())
                ->method('revertCouponUsageForOrder')
                ->with($order);
        } else {
            $couponManagerMock
                ->expects($this->never())
                ->method('revertCouponUsageForOrder');
        }
        $this->replaceByMock('helper', 'wg_customcouponupdate/couponManager', $couponManagerMock);
        return $couponManagerMock;
    }

    /**
     * @param $order
     * @param $isCriticalOrderStateChange bool
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function createIsCriticalOrderStateChangeWgStockHelperMock($order, $isCriticalOrderStateChange)
    {
        $wgStockHelperMock = $this->getMock('Webgriffe_CustomStockUpdate_Helper_Data');
        $wgStockHelperMock
            ->expects($this->once())
            ->method('isCriticalOrderStateChange')
            ->with($order)
            ->will($this->returnValue($isCriticalOrderStateChange));
        $this->replaceByMock('helper', 'wg_stock', $wgStockHelperMock);
        return $wgStockHelperMock;
    }

    /**
     * @param $observerData
     * @param $shouldBeCalled bool
     */
    private function createSalesRuleObserverMock($observerData, $shouldBeCalled)
    {
        $salesRuleObserverMock = $this->getMock('Mage_SalesRule_Model_Observer');
        if ($shouldBeCalled) {
            $salesRuleObserverMock
                ->expects($this->once())
                ->method('sales_order_afterPlace')
                ->with($observerData);
        } else {
            $salesRuleObserverMock
                ->expects($this->never())
                ->method('sales_order_afterPlace');
        }
        $this->replaceByMock('singleton', 'salesrule/observer', $salesRuleObserverMock);
    }
} 