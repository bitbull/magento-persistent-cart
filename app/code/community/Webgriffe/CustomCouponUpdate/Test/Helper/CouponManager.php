<?php
/**
 * @author Manuele Menozzi <mmenozzi@webgriffe.com>
 */

class Webgriffe_CustomCouponUpdate_Test_Helper_CouponManager extends EcomDev_PHPUnit_Test_Case
{
    public function testRevertCouponUsageForOrderShouldDecreaseUsageCounter()
    {
        $order = new Mage_Sales_Model_Order();
        $couponManager = new Webgriffe_CustomCouponUpdate_Helper_CouponManager();
        $couponManager->revertCouponUsageForOrder($order);
        $this->markTestIncomplete();
    }
} 