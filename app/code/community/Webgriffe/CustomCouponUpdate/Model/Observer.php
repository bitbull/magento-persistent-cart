<?php
/**
 * @author Manuele Menozzi <mmenozzi@webgriffe.com>
 */

class Webgriffe_CustomCouponUpdate_Model_Observer
{
    /**
     * @param $observer Varien_Event_Observer
     */
    public function salesOrderPlaceAfter($observer)
    {
        $order = $observer->getEvent()->getOrder();

        /** @var Webgriffe_CustomStockUpdate_Helper_Data $wgStockHelper */
        $wgStockHelper = Mage::helper('wg_stock');
        if ($wgStockHelper->canReduceStock($order)) {
            return;
        }

        /** @var Webgriffe_CustomCouponUpdate_Helper_CouponManager $couponManager */
        $couponManager = Mage::helper('wg_customcouponupdate/couponManager');
        $couponManager->revertCouponUsageForOrder($order);
    }

    /**
     * @param $observer Varien_Event_Observer
     */
    public function salesOrderSaveAfter($observer)
    {
        /** @var Webgriffe_CustomStockUpdate_Helper_Data $wgStockHelper */
        $wgStockHelper = Mage::helper('wg_stock');

        if ($wgStockHelper->isCriticalOrderStateChange($observer->getEvent()->getOrder())) {
            /** @var Mage_SalesRule_Model_Observer $salesRuleObserver */
            $salesRuleObserver = Mage::getSingleton('salesrule/observer');
            $salesRuleObserver->sales_order_afterPlace($observer);
        }
    }
}